package com.seqato.process;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.camel.Body;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;  
import org.slf4j.LoggerFactory;  
public class processing {
public void MessageProcessing(@Body String data)
{
	System.out.println("Inside Class ");
//	Jedis jedis = new Jedis("localhost");
//	System.out.println("JEDIS connection success,, "+jedis.ping());
//      jedis.set("messsage", data); 
//    
//      System.out.println("Stored string in redis:: "+ jedis.get("messsage"));
    Properties properties = new Properties();
    properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

    properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group");
    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

    properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
	KafkaConsumer<String,String> consumer = new KafkaConsumer<String,String>(properties);
	consumer.subscribe(Collections.singleton("test"));

	while (true) {
	    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

	    for (ConsumerRecord<String, String> record : records) {
	        System.out.println("Message received: " + record.value());
	    }
	    consumer.commitAsync();
	}
}
	
}
